const Sequelize = require('sequelize')

module.exports = function (sequelize) {
  const User = sequelize.define('User', {
    email: Sequelize.STRING,
    username: Sequelize.STRING,
    name: Sequelize.STRING,
    password: Sequelize.STRING,
  })

  const Group = sequelize.define('Group', {
    name: Sequelize.STRING,
  })

  Group.belongsToMany(User, { through: 'Groups_Users' })
  User.belongsToMany(Group, { through: 'Users_Groups' })

  return {
    User, Group,
  }
}
