module.exports = `
scalar Date

type Query {
  user(id: ID!): User
  users: [User]
  group(id: ID!): Group
  groups: [Group]
}

type Mutation {
  signup(input: UserInput!): AuthPayload
  login(username: String!, password: String!): AuthPayload
  createGroup(input: GroupInput!): Group
  addUserToGroup(userId: ID!, groupId: ID!): ValidationPayload
  removeUserFromGroup(userId: ID!, groupId: ID!): ValidationPayload
  removeGroup(groupId: ID!): ValidationPayload
}

type User {
  id: ID!
  email: String!
  username: String!
  name: String!
  password: String!
  groups: [Group]
}

input UserInput {
  email: String!
  username: String!
  name: String!
  password: String!
}

type Group {
  id: ID!
  name: String!
  users: [User]
  createdAt: Date!
}

input GroupInput {
  name: String!
}

type AuthPayload {
  token: String!
  user: User!
}

type ValidationPayload {
  valid: Boolean
  error: String
}

union SearchResult = User | Group

schema {
  query: Query
  mutation: Mutation
}
`
