const Sequelize = require('sequelize')
const { models } = require('../../models')

module.exports = {
  async group (root, { id }) {
    const group = await models.Group.findById(id)
    return group
  },
    groups(root, {}, context) {
      return models.Group.findAll({}, context)
    },
  async user (root, { id }) {
    const user = await models.User.findById(id)
    return user
  },
    users(root, {}, context) {
      return models.User.findAll({}, context)
    }
}
