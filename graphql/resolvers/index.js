const GraphQLDate = require('graphql-date')

const query = require('./query')
const mutation = require('./Mutation')

module.exports = function resolvers() {
  return {
    Query: query,

    Mutation: mutation,

    User: {
      groups(user) {
        return user.getGroups()
      }
    },

    Group: {
      users(group) {
        return group.getUsers()
      },
    },

    Date: GraphQLDate
  }
}
