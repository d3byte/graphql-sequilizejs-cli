const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const { models } = require('../../../models')
const Group = require('./group')
const { secret } = require('../../../utils/index')

const auth = {
  async signup(root, { input }, context) {
    console.log(input)
    const { email, name, username, password } = input
    console.log(email, name, username, password)
    const hashedPassword = await bcrypt.hash(password, 10)
    const user = await models.User.create({ email, name, username, password: hashedPassword })
    return {
      token: jwt.sign({ userId: user.id }, secret()),
      user
    }
  },

  async login(root, { username, password }, context) {
    const user = await models.User.findOne({ where: { username } })
    if (!user) {
      return {
        error: 'There are no users with such username'
      }
    }
    const valid = await bcrypt.compare(password, user.password)
    if (!valid) {
      return {
        error: 'Wrong password'
      }
    }
    return {
      token: jwt.sign({ userId: user.id }, secret()),
      user,
    }
  },
}

module.exports = auth