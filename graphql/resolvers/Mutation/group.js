const { models } = require('../../../models')
const { getUserId } = require('../../../utils')

module.exports = {
    createGroup(root, { input, usersIds }, context) {
        return models.Group.create(input)
    },

    async addUserToGroup(root, { userId, groupId }, context) {
        const group = await models.Group.findById(groupId)
        const user = await models.User.findById(userId)
        const addedUser = await group.addUser(userId),
            addedGroup = await user.addGroup(groupId)
        if (addedUser && addedGroup) {
            return { valid: true }
        }
    },
    
    async removeUserFromGroup(root, { userId, groupId }, context) {
        const group = await models.Group.findById(groupId)
        const user = await models.User.findById(userId)
        const removedUser = await group.removeUser(userId),
            removedGroup = await user.removeGroup(groupId)
        if (removedUser && removedGroup) {
            return { valid: true }
        }
    },

    async removeGroup(root, { id }, context) {
        const group = await models.Group.findById(id)
        const done = await group.destroy()
        if (done) {
            return { valid: true }
        }
    }
}